﻿using Mobile_App.Views;
using Nancy.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using userControlsModels.Grids;
using userControlsModels.Model;
using userControlsModels.Utils;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Mobile_App
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MobileDataGrid : Grid
    {
        private DataGrid dataGrid;
        private DataRow[] listOfRows;
        private int numberOfRows = 2;
        private int page = 1;
        private int rowsByPage = 15;

        public MobileDataGrid(DataGrid dataGrid)
        {
            InitializeComponent();
            this.dataGrid = dataGrid;

            CreateGridLayout();
            CreateHeader();
            listOfRows = dataGrid.DataTable.Select();

            var result = listOfRows.Take(rowsByPage);
            this.FillRows(result.ToArray());

        }

        protected void CreateGridLayout()
        {
            List<ColumnDataType> columnDataTypes = dataGrid.ColumnDataTypes;
            int nCol = columnDataTypes.Count;

            this.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            this.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            for (int i = 0; i < rowsByPage; i++)
            {
                this.RowDefinitions.Add(new RowDefinition { Height = 80 });
            }
            this.RowDefinitions.Add(new RowDefinition { Height = 50 });

            for (int i = 0; i < nCol; i++)
            {
                if (columnDataTypes[i].Type != ColumnDataType.Types.Year && columnDataTypes[i].Type != ColumnDataType.Types.ID)
                {
                    this.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
                }
                else
                {
                    this.ColumnDefinitions.Add(new ColumnDefinition { Width = 0 });
                }
            }
            this.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });

        }
        private void CreateHeader()
        {

            List<ColumnDataType> columnDataTypes = dataGrid.ColumnDataTypes;
            int nCol = columnDataTypes.Count;

            for (int i = 0; i < nCol; i++)
            {

                if (columnDataTypes[i].Type != ColumnDataType.Types.Year && columnDataTypes[i].Type != ColumnDataType.Types.ID)
                {
                    this.Children.Add(new Label() { Text = columnDataTypes[i].Name }, columnDataTypes[i].Index, 1);
                }
            }
        }

        protected void FillRows(DataRow[] rows)
        {
            DataTable table = dataGrid.DataTable;
            List<ColumnDataType> columnDataTypes = dataGrid.ColumnDataTypes;

            int nCol = columnDataTypes.Count;
            this.ColumnSpacing = 5;


            Label onPage = new Label()
            {
                Text = "Page " + page,
                FontSize = 16,
                Padding = new Thickness(15, 0, 0, 0),
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Start
            };
            SetColumnSpan(onPage, 2);
            Picker amountOfRows = new Picker() { Title = "Row Amount" };
            amountOfRows.Items.Add("10");
            amountOfRows.Items.Add("20");
            amountOfRows.Items.Add("50");
            amountOfRows.SelectedIndexChanged += AmountOfRows_SelectedIndexChanged;

            Button nextButton = new Button() { Text = "Next" };
            nextButton.Clicked += NextButton_Clicked;
            SetColumnSpan(nextButton, 2);

            Button previousButton = new Button() { Text = "Previous" };
            previousButton.Clicked += PreviousButton_Clicked;

            SearchBar searchBar = new SearchBar() { Placeholder = "Search" };
            searchBar.SearchButtonPressed += SearchBar_SearchButtonPressed;
            SetColumnSpan(searchBar, 5);


            this.Children.Add(searchBar);
            //    this.Children.Add(lastRowStackLayout, 1, rowsByPage + 2);
            this.Children.Add(nextButton, nCol - 1, rowsByPage + 2);
            this.Children.Add(previousButton, 1, rowsByPage + 2);
            this.Children.Add(onPage, 3, rowsByPage + 2);
            this.Children.Add(amountOfRows, 2, rowsByPage + 2);


            foreach (DataRow row in rows)
            {
                //if (numberOfRows < page - 1 * 15 || numberOfRows > page * 15)
                //{
                //    break;
                //}


                if (numberOfRows % 2 != 0)
                {
                    BoxView boxView = new BoxView()
                    {
                        BackgroundColor = Color.FromHex(hex: "F4F4F4")
                    };

                    this.Children.Add(boxView, 0, numberOfRows);
                    SetColumnSpan(boxView, nCol);
                }


                foreach (ColumnDataType column in columnDataTypes)
                {
                    switch (column.Type)
                    {
                        case ColumnDataType.Types.Text:
                            this.Children.Add(new Label()
                            {
                                Text = "" + row[column.Index] as string,
                                VerticalTextAlignment = TextAlignment.Center
                            }, column.Index, numberOfRows);
                            break;
                        case ColumnDataType.Types.TextWithDetails:
                            StackLayout textWithDetailsStackLayout = new StackLayout() { VerticalOptions = LayoutOptions.Center };


                            string[] textWithDetail = ((string)row[column.Index]).Split('-');

                            string mainText = "";
                            string detailText = "";
                            for (int i = 0; i < textWithDetail.Length; i++)
                            {
                                if (i > 0 && i != textWithDetail.Length - 1)
                                {
                                    mainText += " - " + textWithDetail[i] as string;
                                }
                                if (i == textWithDetail.Length - 1)
                                {
                                    detailText += textWithDetail[i] as string;
                                }
                                else
                                {
                                    mainText += textWithDetail[i] as string;
                                }
                            }
                            string suplementalText = "";
                            int lineLength = 10;
                            if (mainText.Length > lineLength)
                            {
                                suplementalText = mainText.Substring(lineLength, (mainText.Length - lineLength));
                                if (suplementalText.Length > lineLength)
                                {
                                    mainText = mainText.Substring(0, lineLength) + "\n" + suplementalText.Substring(0, lineLength);

                                }
                                else
                                {
                                    mainText = mainText.Substring(0, lineLength) + "\n" + suplementalText;
                                }
                            }


                            textWithDetailsStackLayout.Children.Add(new Label()
                            {
                                Text = mainText,
                                VerticalTextAlignment = TextAlignment.Center
                            });
                            textWithDetailsStackLayout.Children.Add(new Label()
                            {
                                Text = detailText,
                                FontSize = 12,
                                VerticalTextAlignment = TextAlignment.Center
                            });

                            this.Children.Add(textWithDetailsStackLayout, column.Index, numberOfRows);
                            break;

                        case ColumnDataType.Types.Image:
                            StackLayout stackLayout = new StackLayout();
                            foreach (string img in row[column.Index] as List<string>)
                            {
                                stackLayout.Children.Add(new Image() { Source = ImageSource.FromUri(new Uri(img)), HeightRequest = 40 });
                            }

                            this.Children.Add(stackLayout, column.Index, numberOfRows);
                            break;
                        case ColumnDataType.Types.Progress:
                            StackLayout stackLayoutProgress = new StackLayout()
                            {
                                VerticalOptions = LayoutOptions.Center
                            };
                            stackLayoutProgress.Children.Add(new ProgressBar
                            {

                                HeightRequest = 30,
                                ProgressColor = Color.LimeGreen,
                                Progress = (double)row[column.Index]
                            });
                            stackLayoutProgress.Children.Add(new Label()
                            {
                                Text = (100 * (double)row[column.Index]).ToString() + "% Completed"
                            });


                            this.Children.Add(stackLayoutProgress, column.Index, numberOfRows);
                            break;
                        case ColumnDataType.Types.Price:

                            this.Children.Add(new Label()
                            {
                                Text = row[column.Index] + "€",
                                VerticalTextAlignment = TextAlignment.Center
                            }, column.Index, numberOfRows);

                            break;
                        case ColumnDataType.Types.Status:
                            bool success = (bool)row[column.Index];

                            Color color;
                            string result = "";
                            if (success)
                            {
                                color = Color.LimeGreen;
                                result = "Success";

                            }
                            else
                            {
                                color = Color.DeepSkyBlue;
                                result = "Running";
                            }

                            Frame frame = new Frame()
                            {
                                WidthRequest = 60,
                                Margin = new Thickness(0, 30, 0, 30),
                                Padding = new Thickness(5, 0, 5, 0),
                                HeightRequest = 30,
                                BackgroundColor = color,
                                CornerRadius = 3
                            };

                            this.Children.Add(frame, column.Index, numberOfRows);

                            this.Children.Add(new Label()
                            {
                                Text = result,
                                TextColor = Color.White,
                                FontAttributes = FontAttributes.Bold,
                                FontSize = 12,
                                VerticalTextAlignment = TextAlignment.Center,
                                HorizontalTextAlignment = TextAlignment.Center
                            }, column.Index, numberOfRows);
                            break;
                        case ColumnDataType.Types.Options:
                            StackLayout optionsStackLayout = new StackLayout();

                            foreach (ColumnDataType.Options option in row[column.Index] as List<ColumnDataType.Options>)
                            {
                                Color optionColor;
                                string optionText;
                                switch (option)
                                {
                                    case ColumnDataType.Options.View:

                                        optionColor = Color.Blue;
                                        optionText = "View";

                                        break;
                                    case ColumnDataType.Options.Edit:

                                        optionColor = Color.Orange;
                                        optionText = "Edit";
                                        break;
                                    case ColumnDataType.Options.Delete:

                                        optionColor = Color.Red;
                                        optionText = "Delete";
                                        break;
                                    default:
                                        optionColor = Color.Red;
                                        optionText = "Unspecified";
                                        break;
                                }
                                optionsStackLayout.Children.Add(new Button()
                                {
                                    WidthRequest = 40,
                                    Padding = new Thickness(5, 0, 5, 0),
                                    HeightRequest = 30,
                                    CornerRadius = 1,
                                    BackgroundColor = optionColor,
                                    TextColor = Color.White,
                                    Text = optionText

                                });

                            }
                            this.Children.Add(optionsStackLayout, column.Index, numberOfRows);
                            break;
                        default:
                            Console.WriteLine("Default case");
                            break;
                    }

                }

                Button buttonDetails = new Button()
                {
                    BackgroundColor = Color.SkyBlue,
                    Text = "Details",
                    BindingContext = listOfRows[numberOfRows - 2],
                    CornerRadius = 5,
                    Margin = new Thickness(5, 17, 5, 17),
                    FontSize = 11,
                    FontAttributes = FontAttributes.Bold
                };
                buttonDetails.Clicked += DetailsButton_Clicked;
                this.Children.Add(buttonDetails, nCol, numberOfRows);
                numberOfRows++;
            }
        }

        private void AmountOfRows_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.numberOfRows = 2;
            this.page = 1;
            rowsByPage = int.Parse((string)((Picker)sender).SelectedItem);
            ClearGrid();
            this.CreateHeader();
            this.FillRows(listOfRows.Take(rowsByPage).ToArray());
        }

        private DataGrid GetDataGrid()
        {

            DataGrid dataGrid = new DataGrid();

            ColumnDataType firstColumn = new ColumnDataType() { Index = 0, Name = "#", Type = ColumnDataType.Types.Text };
            ColumnDataType secondColumn = new ColumnDataType() { Index = 1, Name = "Project", Type = ColumnDataType.Types.TextWithDetails };
            ColumnDataType thirdColumn = new ColumnDataType() { Index = 2, Name = "Team", Type = ColumnDataType.Types.Image };
            ColumnDataType fourthColumn = new ColumnDataType() { Index = 3, Name = "Project Progress", Type = ColumnDataType.Types.Progress };
            ColumnDataType fifthColumn = new ColumnDataType() { Index = 4, Name = "Status", Type = ColumnDataType.Types.Status };
            ColumnDataType sixthColumn = new ColumnDataType() { Index = 5, Name = "Options", Type = ColumnDataType.Types.Options };

            List<ColumnDataType> columnDataTypes = new List<ColumnDataType>
            {
                firstColumn,secondColumn,thirdColumn,fourthColumn,fifthColumn,sixthColumn
            };

            dataGrid.ColumnDataTypes = columnDataTypes;

            DataTable generatedTable = new DataTable();
            generatedTable.Columns.Add("#", typeof(string));
            generatedTable.Columns.Add("Project Name", typeof(string));
            generatedTable.Columns.Add("Team Members", typeof(List<string>));
            generatedTable.Columns.Add("Project Progress", typeof(double));
            generatedTable.Columns.Add("Status", typeof(bool));
            generatedTable.Columns.Add("", typeof(List<ColumnDataType.Options>));

            List<ColumnDataType.Options> options = new List<ColumnDataType.Options>()
            {
                ColumnDataType.Options.View,
                ColumnDataType.Options.Edit
            };

            List<string> images = new List<string>() {
             "https://cdn.iconscout.com/icon/free/png-256/avatar-380-456332.png",
                "https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png"
            };

            generatedTable.Rows.Add("#", "Proj0", images, "0", false, options);
            generatedTable.Rows.Add("#", "Proj1", images, "0.25", false, options);
            generatedTable.Rows.Add("#", "Proj2", images, "0.50", true, options);
            generatedTable.Rows.Add("#", "Proj3", images, "0.75", true, options);
            generatedTable.Rows.Add("#", "Proj4", images, "0.9", true, options);
            generatedTable.Rows.Add("#", "Proj5", images, "1", true, options);
            generatedTable.Rows.Add("#", "Proj6", images, "1", true, options);
            generatedTable.Rows.Add("#", "Proj7", images, "1", true, options);
            generatedTable.Rows.Add("#", "Proj8", images, "1", true, options);

            dataGrid.DataTable = generatedTable;

            return dataGrid;
        }

        private void NextButton_Clicked(object sender, EventArgs e)
        {
            numberOfRows = 2;
            this.page++;
            ClearGrid();
            CreateHeader();
            var result = listOfRows.Skip(page * rowsByPage).Take(rowsByPage);
            this.FillRows(result.ToArray());
        }

        private void PreviousButton_Clicked(object sender, EventArgs e)
        {
            if (page != 1)
            {
                numberOfRows = 2;
                this.page--;
                ClearGrid();
                CreateHeader();
                var result = listOfRows.Skip((page - 1) * rowsByPage).Take(rowsByPage);
                this.FillRows(result.ToArray());
            }
        }

        private void SearchBar_SearchButtonPressed(object sender, EventArgs e)
        {
            this.numberOfRows = 2;
            this.page = 1;
            ClearGrid();
            this.CreateHeader();

            string searchString = ((SearchBar)sender).Text;
            DataRow[] result;

            if (searchString.Equals(""))
            {
                this.listOfRows = dataGrid.DataTable.Select();
            }
            else
            {
                this.listOfRows = dataGrid.DataTable.Select("Product_name LIKE '%" + searchString + "%' OR Brand_name LIKE '%" + searchString + "%' OR Category_name LIKE '%" + searchString + "%'");
            }

            result = this.listOfRows.Take(rowsByPage).ToArray();
            this.FillRows(result);
        }

        private void ClearGrid()
        {
            this.Children.Clear();
        }

        private async void DetailsButton_Clicked(object sender, EventArgs e)
        {
            Button buttonDetails = (Button)sender;

            DataRow row = (DataRow) buttonDetails.BindingContext;
            await PopupNavigation.Instance.PushAsync(new PopUpDetailsView(row, dataGrid.ColumnDataTypes.ToArray())).ConfigureAwait(false);
        }
    }

}
