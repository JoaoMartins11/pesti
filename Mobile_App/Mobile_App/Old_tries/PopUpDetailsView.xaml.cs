﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using userControlsModels.Grids;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;

namespace Mobile_App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpDetailsView
    {
        public DataRow row;
        public ColumnDataType[] columnDataTypes;

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string product;

        public string Product
        {
            get { return product; }
            set { product = value; }
        }

        private string brand;

        public string Brand
        {
            get { return brand; }
            set { brand = value; }
        }

        private string category;

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        private int year;

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        private double price;

        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        public PopUpDetailsView(DataRow row, ColumnDataType[] columnDataTypes)
        {
            InitializeComponent();
            this.BindingContext = this;

            InitializeData(row, columnDataTypes);
            foreach (ColumnDataType column in columnDataTypes)
            {
                switch (column.Name)
                {

                    case "Id":
                        EntryCell idEntry = new EntryCell
                        {
                            Label = column.Name,
                            Keyboard = Keyboard.Numeric
                        };
                        idEntry.SetBinding(EntryCell.TextProperty, nameof(this.Id), mode: BindingMode.TwoWay);
                        this.tableSection.Add(idEntry);
                        break;
                    case "Product":
                        EntryCell productEntry = new EntryCell
                        {
                            Label = column.Name,
                            Keyboard = Keyboard.Text
                        };
                        productEntry.SetBinding(EntryCell.TextProperty, nameof(this.Product), mode: BindingMode.TwoWay);
                        this.tableSection.Add(productEntry);

                        break;

                    case "Brand":
                        EntryCell entryCellBrand = new EntryCell
                        {
                            Label = column.Name,
                            Keyboard = Keyboard.Text
                        };
                        entryCellBrand.SetBinding(EntryCell.TextProperty, nameof(this.Brand), mode: BindingMode.TwoWay);
                        this.tableSection.Add(entryCellBrand);
                        break;

                    case "Category":
                        EntryCell entryCellCategory = new EntryCell
                        {
                            Label = column.Name,
                            Keyboard = Keyboard.Text
                        };
                        entryCellCategory.SetBinding(EntryCell.TextProperty, nameof(this.Category), mode: BindingMode.TwoWay);
                        this.tableSection.Add(entryCellCategory);
                        break;

                    case "Year":
                        EntryCell yearEntry = new EntryCell
                        {
                            Label = column.Name,
                            Keyboard = Keyboard.Numeric
                        };
                        yearEntry.SetBinding(EntryCell.TextProperty, nameof(this.Year), mode: BindingMode.TwoWay);
                        this.tableSection.Add(yearEntry);
                        break;

                    case "Price":
                        EntryCell priceEntry = new EntryCell
                        {
                            Label = column.Name,
                            Keyboard = Keyboard.Numeric
                        };
                        priceEntry.SetBinding(EntryCell.TextProperty, nameof(this.Price), mode: BindingMode.TwoWay);
                        this.tableSection.Add(priceEntry);
                        break;
                }
            }


            StackLayout buttonStackLayout = new StackLayout()
            {
                Padding = 12,
                BackgroundColor = Color.White,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.Center,
                Orientation = StackOrientation.Horizontal
            };

            Button updateButton = new Button()
            {
                BackgroundColor = Color.SkyBlue,
                Text = "Update",
                CornerRadius = 5,
                Margin = new Thickness(5, 17, 5, 17),
                FontSize = 14,
                HorizontalOptions = LayoutOptions.Center
            };

            Button closeButton = new Button()
            {
                BackgroundColor = Color.IndianRed,
                Text = "Close",
                CornerRadius = 5,
                Margin = new Thickness(5, 17, 5, 17),
                FontSize = 14,
                HorizontalOptions = LayoutOptions.Center
            };
            closeButton.Clicked += CloseButton_Clicked;
            updateButton.Clicked += UpdateButton_Clicked;

            buttonStackLayout.Children.Add(updateButton);
            buttonStackLayout.Children.Add(closeButton);
            this.layout.Children.Add(buttonStackLayout);
        }

        private void InitializeData(DataRow row, ColumnDataType[] columnDataTypes)
        {
            foreach (ColumnDataType column in columnDataTypes)
            {
                switch (column.Name)
                {

                    case "Id":
                        this.Id = int.Parse("" + row[column.Index]);
                        break;
                    case "Product":
                        this.Product = (string)row[column.Index];
                        break;
                    case "Brand":
                        this.brand = (string)row[column.Index];
                        break;
                    case "Category":
                        this.Category = (string)row[column.Index];
                        break;

                    case "Year":
                        this.Year = (int)row[column.Index];
                        break;

                    case "Price":
                        string priceString = "" + row[column.Index];
                        this.Price = double.Parse(priceString);
                        break;
                }
            }
        }

        private void UpdateButton_Clicked(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void CloseButton_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
    }
}