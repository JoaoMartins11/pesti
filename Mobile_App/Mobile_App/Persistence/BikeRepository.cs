﻿using Microsoft.AspNetCore.WebUtilities;
using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using userControlsModels.Model;

namespace Mobile_App.Persistence
{
    public class BikeRepository
    {

        public ObservableCollection<Bike> GetAllBikes()
        {
            ObservableCollection<Bike> bikeList = new ObservableCollection<Bike>();

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/values");

            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            bikeList = js.Deserialize<ObservableCollection<Bike>>(webcontent);

            return bikeList;
        }

        public HttpStatusCode UpdateBike(Bike bike)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/values");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            JavaScriptSerializer js = new JavaScriptSerializer();

            js.Serialize(bike);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = js.Serialize(bike);

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                return httpResponse.StatusCode;
            }

        }

    }

}