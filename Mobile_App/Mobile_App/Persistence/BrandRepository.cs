﻿using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using userControlsModels.Bikes;
using userControlsModels.Model;

namespace Mobile_App.Persistence
{
    public class BrandRepository
    {

        public ObservableCollection<Brand> GetAllBrands()
        {
            ObservableCollection<Brand> brandList = new ObservableCollection<Brand>();

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/brand");

            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            brandList = js.Deserialize<ObservableCollection<Brand>>(webcontent);

            return brandList;
        }

        public void UpdateBrand(Brand brand)
        {

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/brand");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            JavaScriptSerializer js = new JavaScriptSerializer();

            js.Serialize(brand);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = js.Serialize(brand);

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

        }

    }

}