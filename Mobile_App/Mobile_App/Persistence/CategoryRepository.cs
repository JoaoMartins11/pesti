﻿using Nancy.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using userControlsModels.Bikes;
using userControlsModels.Model;

namespace Mobile_App.Persistence
{
    public class CategoryRepository
    {

        public ObservableCollection<Category> GetAllCategories()
        {
            ObservableCollection<Category> categoryList = new ObservableCollection<Category>();

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/category");

            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            categoryList = js.Deserialize<ObservableCollection<Category>>(webcontent);

            return categoryList;
        }

        public void UpdateCategory(Category category)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/category");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            JavaScriptSerializer js = new JavaScriptSerializer();

            js.Serialize(category);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = js.Serialize(category);

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }

        }

    }

}