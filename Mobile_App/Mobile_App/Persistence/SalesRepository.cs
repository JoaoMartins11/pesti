﻿using Mobile_App.model;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using userControlsModels.Bikes;
using userControlsModels.Model;

namespace Mobile_App.Persistence
{
    public class SalesRepository
    {

        public ObservableCollection<Sale> GetSales(int year)
        {
            ObservableCollection<Sale> salesList = new ObservableCollection<Sale>();

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/Sales/SalesByYear?yearSales=" + year);

            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }

            var values = JsonConvert.DeserializeObject<ObservableCollection<Sale>>(webcontent);

        

            return values;
        }


        public string GetYearRevenue(int year)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/sales?year=" + year);

            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            string revenue = js.Deserialize<string>(webcontent);
            return revenue;
        }

        public List<MonthRevenue> GetMonthlyRevenue(int year)
        {


            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/sales/bymonth?yearByMonth=" + year);

            var response = req.GetResponse();
            string webcontent;
            using (var strm = new StreamReader(response.GetResponseStream()))
            {
                webcontent = strm.ReadToEnd();
            }

            var values = JsonConvert.DeserializeObject<List<MonthRevenue>>(webcontent);


            return values;
        }
    }

}