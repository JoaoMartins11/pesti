﻿using Mobile_App.model;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using userControlsModels.Bikes;
using userControlsModels.Model;
using userControlsModels.Sales;

namespace Mobile_App.Persistence
{
    public class StaffRepository
    {
        public static Staff Get(int id)
        {
            Staff Staff = new Staff();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/Staff?id=" + id);

                var response = req.GetResponse();
                string webcontent;
                using (var strm = new StreamReader(response.GetResponseStream()))
                {
                    webcontent = strm.ReadToEnd();
                }

                Staff = JsonConvert.DeserializeObject<Staff>(webcontent);

            }
            catch { };

            return Staff;
        }
    }


}