﻿using Mobile_App.model;
using Nancy.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using userControlsModels.Bikes;
using userControlsModels.Model;
using userControlsModels.Sales;

namespace Mobile_App.Persistence
{
    public class StoreRepository
    {
        public static Store Get(int id)
        {
            Store Store = new Store();

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/Store?id=" + id);

                var response = req.GetResponse();
                string webcontent;
                using (var strm = new StreamReader(response.GetResponseStream()))
                {
                    webcontent = strm.ReadToEnd();
                }

                Store = JsonConvert.DeserializeObject<Store>(webcontent);

            }
            catch { };

            return Store;
        }
    }


}