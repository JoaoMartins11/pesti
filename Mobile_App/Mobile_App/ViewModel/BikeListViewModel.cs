﻿
using GalaSoft.MvvmLight;
using Mobile_App.Persistence;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using userControlsModels.Model;

namespace Mobile_App.ViewModel
{
    public class BikeListViewModel : ViewModelBase
    {

        private BikeRepository bikeRepository;
        private int page = 1;
        private int rowNumberByPage = 10;
        public ObservableCollection<Bike> BikeList { get; set; }

        public ObservableCollection<Bike> FullBikeList { get; set; }


        public BikeListViewModel()
        {
            this.bikeRepository = new BikeRepository();

            this.FullBikeList = bikeRepository.GetAllBikes();
            this.BikeList = new ObservableCollection<Bike>(FullBikeList.Take(page * rowNumberByPage));
        }

        public void Next()
        {
            foreach (Bike b in FullBikeList.Take(rowNumberByPage))
            {
                this.BikeList.Add(b);
            }
            this.page++;

        }

        internal void Searched(string keyword)
        {
            this.page = 1;
            FullBikeList = new ObservableCollection<Bike>(bikeRepository.GetAllBikes().Where(Bike => Bike.Product_name.Contains(keyword) || Bike.Brand_name.Contains(keyword) || Bike.Category_name.Contains(keyword) || Bike.List_price.ToString(CultureInfo.InvariantCulture.NumberFormat).Contains(keyword)));
            Previous();
        }

        public void Previous()
        {
            if (page > 1)
            {
                this.page--;
            }
            this.BikeList.Clear();
            foreach (Bike b in FullBikeList.Skip(page - 1 * rowNumberByPage).Take(rowNumberByPage))
            {
                this.BikeList.Add(b);
            }

        }



    }
}

