﻿
using GalaSoft.MvvmLight;
using System;

namespace Mobile_App.ViewModel
{
    public class HeaderViewModel : ViewModelBase
    {
        
        public string Balance { get; set; }

        public HeaderViewModel()
        {
            UpdateBalance(0);
        }


        private void UpdateBalance(double value)
        {
            value = Math.Truncate(100 * value) / 100;

            Balance = value + " €";
        }
    }
}
