﻿
using GalaSoft.MvvmLight;
using Mobile_App.Persistence;
using System.Collections.ObjectModel;
using userControlsModels.Bikes;
using userControlsModels.Model;

namespace Mobile_App.ViewModel
{
    public class PopUpEditViewModel : ViewModelBase
    {
        private BrandRepository brandRepository;
        private CategoryRepository categoryRepository;
        public ObservableCollection<Brand> BrandList { get; }
        public ObservableCollection<Category> CategoryList { get;}
        public Brand SelectedBrand { get; set; }
        public Category SelectedCategory { get; set; }
        public Bike EditedBike { get; set; }
        public string Message { get; set; }

        public PopUpEditViewModel(Bike bike)
        {
            this.EditedBike = bike;
            this.brandRepository = new BrandRepository();
            this.categoryRepository = new CategoryRepository();

            this.BrandList = brandRepository.GetAllBrands();
            this.CategoryList = categoryRepository.GetAllCategories();
            this.SelectedBrand = FindSelectedBrandByName(EditedBike.Brand_name);
            this.SelectedCategory = FindSelectedCategoryByName(EditedBike.Category_name);
        }

        private Brand FindSelectedBrandByName(string brand_name)
        {
            foreach (Brand brand in BrandList)
            {
                if (brand.Brand_name.Equals(brand_name, System.StringComparison.Ordinal))
                {
                    return brand;
                }
            }
            return new Brand { Brand_name = "Not Found" };
        }
        private Category FindSelectedCategoryByName(string category_name)
        {
            foreach (Category category in CategoryList)
            {
                if (category.Category_name.Equals(category_name, System.StringComparison.Ordinal))
                {
                    return category;
                }
            }
            return new Category { Category_name = "Not Found" };
        }

        public void UpdateMessage(string msg)
        {
            this.Message = msg;
            RaisePropertyChanged(nameof(Message));
        }

    }
}

