﻿using GalaSoft.MvvmLight;
using Microcharts;
using Mobile_App.model;
using Mobile_App.Persistence;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using userControlsModels.Bikes;

namespace Mobile_App.ViewModel
{
    public class StatisticsViewModel : ViewModelBase
    {

        SalesRepository salesRepository;

        public Chart YearlySpendingsChart { get; set; }

        private ObservableCollection<MonthRevenue> monthlyRevenue = new ObservableCollection<MonthRevenue>();

        public ObservableCollection<Sale> SalesList { get; set; }

        public ObservableCollection<Sale> FullSalesList { get; set; }


        public string YearProfit
        {
            get;
            set;
        }
        public string YearRevenue
        {
            get;
            set;
        }

        public int Year
        {
            get;
            set;
        }

        public ObservableCollection<int> YearList { get; }

        private float maxValue = 0f;
        private int page = 1;
        private int rowNumberByPage = 10;


        public StatisticsViewModel()
        {
            this.YearList = new ObservableCollection<int>();
            this.Year = DateTime.Now.Year;
            this.salesRepository = new SalesRepository();
            UpdateData();
            UpdateYearList();
        }

        private void UpdateSalesList()
        {
            this.FullSalesList = salesRepository.GetSales(this.Year);
            this.SalesList = new ObservableCollection<Sale>(FullSalesList.Take(page * rowNumberByPage));
            RaisePropertyChanged(nameof(FullSalesList));
            RaisePropertyChanged(nameof(SalesList));
        }

        private void UpdateYearList()
        {
            YearList.Clear();
            DateTime localDate = DateTime.Now;
            for (int i = localDate.Year; i > localDate.Year - 100; i--)
            {
                YearList.Add(i);
            }
        }
        public void UpdateData()
        {
            string revenue = salesRepository.GetYearRevenue(this.Year);
            double amount = 0;
            try { amount = double.Parse(revenue, CultureInfo.InvariantCulture.NumberFormat); }
            catch { }

            maxValue = 1;
            this.YearRevenue = amount.ToString("#,#.##€;#,#.##€;0€");
            this.YearProfit = (0.23 * amount).ToString("#,#.##€;#,#.##€;0€");

            ConvertToObservablePair(salesRepository.GetMonthlyRevenue(this.Year));

            YearlySpendingsChart = GetYearlySpendingsChart();

            RaisePropertyChanged(nameof(Year));
            RaisePropertyChanged(nameof(YearlySpendingsChart));
            RaisePropertyChanged(nameof(YearProfit));
            RaisePropertyChanged(nameof(YearRevenue));

            UpdateSalesList();

        }

        private void UpdateMaxValue(float value)
        {
            if (maxValue < value)
            {
                maxValue = value;
            }
        }

        private Chart GetYearlySpendingsChart()
        {
            ObservableCollection<Entry> entries = new ObservableCollection<Entry>();

            for (int i = 1; i <= 12; i++)
            {
                bool found = false;
                float revenue = 0;
                foreach (MonthRevenue monthRevenue in monthlyRevenue)
                {
                    if (monthRevenue.month == i)
                    {
                        revenue = monthRevenue.revenue;
                    }
                }


                UpdateMaxValue(revenue);

                switch (i)
                {
                    case (int)DateUtils.Month.January:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Jan",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;

                    case (int)DateUtils.Month.February:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Feb",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.March:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Mar",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.April:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Apr",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.May:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "May",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.June:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Jun",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.July:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Jul",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.August:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Aug",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.September:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Sep",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.October:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Oct",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.November:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Nov",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    case (int)DateUtils.Month.December:
                        entries.Add(new Microcharts.Entry(revenue)
                        {
                            Color = SKColor.Parse("#FFFFFF"),
                            Label = "Dec",
                            ValueLabel = ConvertToStringFormat(revenue),
                            TextColor = SKColor.Parse("#FFFFFF")
                        });
                        break;
                    default:
                        break;
                }

            }
            // return MonthChart.Chart = new DonutChart { Entries = entries, HoleRadius = 0.4f, BackgroundColor = SKColor.Parse("#ECEEFF"), LabelTextSize = 40 };

            return new LineChart { Entries = entries, PointSize = 20, LineMode = LineMode.Straight, BackgroundColor = SKColors.Transparent, LabelTextSize = 33, MaxValue = 1.1f * maxValue };
        }


        private string ConvertToStringFormat(double amount)
        {
            string amountString;
            if (amount >= 1000 && amount <= 1000000)
            {
                amountString = (amount / 1000).ToString("#,##.## K");

            }
            else if (amount >= 1000000)
            {
                amountString = (amount / 1000000).ToString("#,##.## M");
            }else if (amount == 0)
            {
                amountString = "0.00";
            }
            else { amountString = amount.ToString("#,##.##"); }

            return amountString;
        }

        private void ConvertToObservablePair(List<MonthRevenue> list)
        {

            monthlyRevenue.Clear();
            foreach (MonthRevenue monthRevenue in list)
            {
                monthlyRevenue.Add(monthRevenue);
            }
        }


        public void Next()
        {
            foreach (Sale sale in FullSalesList.Skip(page * rowNumberByPage).Take(rowNumberByPage))
            {
                this.SalesList.Add(sale);
            }
            this.page++;
        }

        internal void Searched(string keyword)
        {
            this.page = 1;
            FullSalesList = new ObservableCollection<Sale>(salesRepository.GetSales(this.Year).Where(Sales => Sales.ProductName.Contains(keyword) || Sales.Price.ToString(CultureInfo.InvariantCulture.NumberFormat).Contains(keyword) || Sales.Quantity.ToString(CultureInfo.InvariantCulture.NumberFormat).Contains(keyword) || Sales.SaleDate.ToString("0:dd MMMM, yyyy").Contains(keyword)));
            Previous();
        }



        public void Previous()
        {
            if (page > 1)
            {
                this.page--;
            }
            this.SalesList.Clear();
            foreach (Sale sale in FullSalesList.Skip(page - 1 * rowNumberByPage).Take(rowNumberByPage))
            {
                this.SalesList.Add(sale);
            }

        }

        public void ResetPage()
        {
            this.SalesList.Clear();
            foreach (Sale sale in FullSalesList.Take(rowNumberByPage))
            {
                this.SalesList.Add(sale);
            }

        }
    }
}
