﻿using Mobile_App.model;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using userControlsModels.Bikes;
using userControlsModels.Model;
using Xamarin.Forms;

namespace Mobile_App.Views
{
    public class AlternateColorDataSalesTemplateSelector : DataTemplateSelector
    {
        public DataTemplate EvenTemplate { get; set; }
        public DataTemplate UnevenTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((ObservableCollection<Sale>)((ListView)container).ItemsSource).IndexOf(item as Sale) % 2 == 0 ? EvenTemplate : UnevenTemplate;
        }
    }
}
