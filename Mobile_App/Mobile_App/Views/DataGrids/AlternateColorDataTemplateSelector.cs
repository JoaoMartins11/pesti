﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using userControlsModels.Model;
using Xamarin.Forms;

namespace Mobile_App.Views
{
    public class AlternateColorDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate EvenTemplate { get; set; }
        public DataTemplate UnevenTemplate { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            return ((ObservableCollection<Bike>)((ListView)container).ItemsSource).IndexOf(item as Bike) % 2 == 0 ? EvenTemplate : UnevenTemplate;
        }
    }
}
