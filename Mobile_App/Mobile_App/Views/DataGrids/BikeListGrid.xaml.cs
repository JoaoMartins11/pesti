﻿using Mobile_App.Views;
using Mobile_App.ViewModel;
using Rg.Plugins.Popup.Services;
using System;
using userControlsModels.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mobile_App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BikeListGrid : ContentView
    {

        private int count = 1;
        public BikeListGrid()
        {
            InitializeComponent();
            this.BindingContext = new BikeListViewModel();

            this.listView.ChildAdded += ListView_ChildAdded;

        }

        private void ListView_ChildAdded(object sender, ElementEventArgs e)
        {
            count++;
            if (count % 2 == 0)
            {
                ((ListView)sender).BackgroundColor = Color.Blue;
            }
        }

        private void searchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = searchBar.Text;
            ((BikeListViewModel)BindingContext).Searched(keyword);
        }




        private void listView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Bike bike = e.Item as Bike;
            PopupNavigation.Instance.PushAsync(new PopUpEditView(bike)).ConfigureAwait(false);
        }

        private void LoadMoreButton_Clicked(object sender, EventArgs e)
        {
            ((BikeListViewModel)BindingContext).Next();
        }
    }
}