﻿using Mobile_App.Views;
using Mobile_App.ViewModel;
using Rg.Plugins.Popup.Services;
using System;
using userControlsModels.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Mobile_App.model;

namespace Mobile_App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SalesListGrid : ContentView
    {

        public SalesListGrid(StatisticsViewModel viewModel )
        {
            InitializeComponent();
            this.BindingContext = viewModel;
        }

        private void searchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            var keyword = searchBar.Text;
            ((StatisticsViewModel)BindingContext).Searched(keyword);
           }

        private void NextButton_Clicked(object sender, EventArgs e)
        {
            ((StatisticsViewModel)BindingContext).Next();
        }

        private void PreviousButton_Clicked(object sender, EventArgs e)
        {
            ((StatisticsViewModel)BindingContext).Previous();
        }
    }
}