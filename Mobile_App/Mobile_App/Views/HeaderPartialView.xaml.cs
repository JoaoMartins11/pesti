﻿using eliteKit.eliteElements;
using Mobile_App.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mobile_App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HeaderPartialView : eliteHeader
    {
        public HeaderPartialView()
        {

            this.BindingContext = new HeaderViewModel();
            InitializeComponent();
        }
    }
}