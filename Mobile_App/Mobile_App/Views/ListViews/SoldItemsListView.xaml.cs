﻿using Mobile_App.ViewModel;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using userControlsModels.Bikes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mobile_App.Views.ListViews
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SoldItemsListView : ContentView
    {
        private StatisticsViewModel viewModel;
        public SoldItemsListView(StatisticsViewModel viewModel)
        {
            InitializeComponent();
            this.viewModel = viewModel;
            this.BindingContext = this.viewModel;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            this.viewModel.Next();
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {

            Sale sale = e.Item as Sale;
            PopupNavigation.Instance.PushAsync(new PopUpSaleDetailsView(sale)).ConfigureAwait(false);


        }
    }
}