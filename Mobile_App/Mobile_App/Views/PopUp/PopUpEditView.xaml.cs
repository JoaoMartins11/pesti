﻿using System;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;
using userControlsModels.Model;
using Mobile_App.Persistence;
using System.Collections;
using System.Collections.Generic;
using userControlsModels.Bikes;
using System.Collections.ObjectModel;
using System.Linq;
using Mobile_App.ViewModel;
using System.Net;

namespace Mobile_App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpEditView
    {

        public PopUpEditView(Bike bikeSent)
        {
            InitializeComponent();

            this.BindingContext = new PopUpEditViewModel(bikeSent);

            /////////////////////////////////////////////////
            EntryCell idEntry = new EntryCell
            {
                Label = "Id",
                Keyboard = Keyboard.Numeric,
                IsEnabled = false
            };
            idEntry.SetBinding(EntryCell.TextProperty, "EditedBike.Product_id");
            this.tableSection.Add(idEntry);

            /////////////////////////////////////////////////
            EntryCell productEntry = new EntryCell
            {
                Label = "Product",
                Keyboard = Keyboard.Text
            };
            productEntry.SetBinding(EntryCell.TextProperty, "EditedBike.Product_name", mode: BindingMode.TwoWay);
            this.tableSection.Add(productEntry);

            /////////////////////////////////////////////////

            ViewCell brandEntry = new ViewCell();


            StackLayout stackBrand = new StackLayout { HorizontalOptions=LayoutOptions.FillAndExpand, Orientation = StackOrientation.Horizontal , Padding=new Thickness(14,0,14,0)};
            stackBrand.Children.Add(new Label {VerticalTextAlignment=TextAlignment.Center, Text = "Brand" });
            
            var picker = new Picker { HorizontalOptions = LayoutOptions.FillAndExpand };
            picker.SetBinding(Picker.ItemsSourceProperty, "BrandList");

            picker.ItemDisplayBinding = new Binding("Brand_name");
            picker.SetBinding(Picker.SelectedItemProperty, "SelectedBrand");

            stackBrand.Children.Add(picker);
            brandEntry.View = stackBrand;
            this.tableSection.Add(brandEntry);

            /////////////////////////////////////////////////
            ViewCell categoryEntry = new ViewCell();

            StackLayout stackCategory = new StackLayout { HorizontalOptions = LayoutOptions.FillAndExpand, Orientation = StackOrientation.Horizontal, Padding = new Thickness(14, 0, 14, 0) };
            stackCategory.Children.Add(new Label { VerticalTextAlignment = TextAlignment.Center, Text = "Category" });

            var pickerCategory = new Picker { HorizontalOptions = LayoutOptions.FillAndExpand };
            pickerCategory.SetBinding(Picker.ItemsSourceProperty, "CategoryList");

            pickerCategory.ItemDisplayBinding = new Binding("Category_name");
            pickerCategory.SetBinding(Picker.SelectedItemProperty, "SelectedCategory");

            stackCategory.Children.Add(pickerCategory);
            categoryEntry.View = stackCategory;
            this.tableSection.Add(categoryEntry);

            /////////////////////////////////////////////////
            EntryCell yearEntry = new EntryCell
            {
                Label = "Year",
                Keyboard = Keyboard.Numeric
            };
            yearEntry.SetBinding(EntryCell.TextProperty, "EditedBike.Model_year", mode: BindingMode.TwoWay);
            this.tableSection.Add(yearEntry);

            /////////////////////////////////////////////////
            EntryCell priceEntry = new EntryCell
            {
                Label = "Price",
                Keyboard = Keyboard.Numeric
            };

            priceEntry.SetBinding(EntryCell.TextProperty, "EditedBike.List_price", mode: BindingMode.TwoWay);
            this.tableSection.Add(priceEntry);
        }

        private void UpdateButton_Clicked(object sender, EventArgs e)
        {
            BikeRepository bikeRepository = new BikeRepository();
            PopUpEditViewModel viewModel = ((PopUpEditViewModel)this.BindingContext);
            viewModel.EditedBike.Brand_name = viewModel.SelectedBrand.Brand_name;
            viewModel.EditedBike.Category_name = viewModel.SelectedCategory.Category_name;

            HttpStatusCode result= bikeRepository.UpdateBike(viewModel.EditedBike);
            string msg = "";
            string successMsg = "Updated Successfully";
            string failMsg = "Unable to Update";
            switch (result)
            {
                case HttpStatusCode.Accepted:
                    msg = successMsg;
                    break;
                case HttpStatusCode.BadGateway:
                    msg = failMsg;
                    break;
                case HttpStatusCode.BadRequest:
                    msg = failMsg;
                    break;
                case HttpStatusCode.Forbidden:
                    msg = failMsg;
                    break;
                case HttpStatusCode.NoContent:
                    msg = successMsg;
                    break;
                case HttpStatusCode.OK:
                    msg = successMsg;
                    break;
                case HttpStatusCode.RequestTimeout:
                    msg = failMsg;
                    break;
                case HttpStatusCode.Unauthorized:
                    msg = failMsg;
                    break;
                default:
                    break;
            }
            viewModel.UpdateMessage(msg);
        }

        private void CloseButton_Clicked(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
    }
}