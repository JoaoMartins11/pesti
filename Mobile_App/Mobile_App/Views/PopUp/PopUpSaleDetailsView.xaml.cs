﻿using System;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;
using userControlsModels.Model;
using Mobile_App.Persistence;
using System.Collections;
using System.Collections.Generic;
using userControlsModels.Bikes;
using System.Collections.ObjectModel;
using System.Linq;
using Mobile_App.ViewModel;
using System.Net;

namespace Mobile_App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpSaleDetailsView
    {

        public PopUpSaleDetailsView(Sale sale)
        {
            InitializeComponent();

            this.BindingContext = sale; // new PopUpSaleDetailsViewModel(sale);

            /////////////////////////////////////////////////
        }
    }
}