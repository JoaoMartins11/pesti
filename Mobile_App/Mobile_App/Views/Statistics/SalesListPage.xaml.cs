﻿using Mobile_App.ViewModel;
using Mobile_App.Views.ListViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mobile_App.Views.Statistics
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SalesListPage : ContentPage
    {
        public SalesListPage(StatisticsViewModel viewModel)
        {

            InitializeComponent();
            ((NavigationPage)Application.Current.MainPage).BarBackgroundColor = Color.CornflowerBlue;

            //  mainStackLayout.Children.Add(new SalesListGrid(viewModel));
            SoldItemsListView soldItemsView = new SoldItemsListView(viewModel);
            mainStackLayout.Children.Add(soldItemsView);
        }





    }
}