﻿using System.ComponentModel;
using Xamarin.Forms;
using Mobile_App.ViewModel;
using Mobile_App.Views.Statistics;

namespace Mobile_App.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class StatisticsPage : ContentPage
    {
        public StatisticsPage()
        {
            
            InitializeComponent();
            this.BindingContext = new StatisticsViewModel();

            this.ChartStackLayout.GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new Command(() => { ((StatisticsViewModel)this.BindingContext).ResetPage();
                    Navigation.PushAsync(new SalesListPage((StatisticsViewModel)this.BindingContext)); }) 
            });


          }

        private void Picker_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ((StatisticsViewModel)this.BindingContext).UpdateData();
        }
    }
}

