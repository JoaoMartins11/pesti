﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mobile_App.model
{
    public class MonthRevenue
    {
        public int month { get; set; }
        public float revenue{ get; set; }
    }
}
