﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nancy.Json;
using userControlsModels.Grids;
using userControlsModels.Model;
using userControlsModels.Utils;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public const string SessionPageNumber = "_Page";
        public const string SessionPageLength = "_PageLength";
        public const string SessionName = "_Name";
        public const string SessionSearch = "_search";
        public DataRow selectedRow;

        public string SessionInfo_Page { get; private set; }
        public string SessionInfo_PageLength { get; private set; }
        public string SessionInfo_Name { get; private set; }
        public string SessionInfo_Search { get; private set; }
        public int lastPageLength = 20;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        public IActionResult SaveDetails(string pname, string category, string brand, string year, string price)
        {

            //can send some data also.
            //this.fname += "...";
            //HtmlDocument.GetElementById("fname");

            return Content("<h1>Information: " + pname + " , " + category + " , " + brand + " , " + year + " , " + price + "</h1>", "text/html");

        }

        public IActionResult Index(string searchTerms, string pageNumber, string pageLength, string searchTermsReset)
        {
            DataGrid dataGrid = new DataGrid();
            ColumnDataType columnDataTypeInt = new ColumnDataType();
            columnDataTypeInt.Name = "String";
            columnDataTypeInt.Type = ColumnDataType.Types.Text;
            columnDataTypeInt.Index = 0;
            ColumnDataType columnDataTypePrice = new ColumnDataType();
            columnDataTypePrice.Name = "String";
            columnDataTypePrice.Type = ColumnDataType.Types.Price;
            columnDataTypePrice.Index = 7;
            ColumnDataType columnDataTypeText = new ColumnDataType();
            columnDataTypeText.Name = "String";
            columnDataTypeText.Type = ColumnDataType.Types.TextWithDetails;
            columnDataTypeText.Index = 0;
            ColumnDataType columnDataTypeImage = new ColumnDataType();
            columnDataTypeImage.Name = "String";
            columnDataTypeImage.Type = ColumnDataType.Types.Image;
            columnDataTypeImage.Index = 1;
            ColumnDataType columnDataTypeProgress = new ColumnDataType();
            columnDataTypeProgress.Name = "String";
            columnDataTypeProgress.Type = ColumnDataType.Types.Progress;
            columnDataTypeProgress.Index = 1;
            ColumnDataType columnDataTypeStatus = new ColumnDataType();
            columnDataTypeStatus.Name = "String";
            columnDataTypeStatus.Type = ColumnDataType.Types.Status;
            columnDataTypeStatus.Index = 1;
            ColumnDataType columnDataTypeOptions = new ColumnDataType();
            columnDataTypeOptions.Name = "String";
            columnDataTypeOptions.Type = ColumnDataType.Types.Options;
            columnDataTypeOptions.Index = 1;
            dataGrid.ColumnDataTypes = new List<ColumnDataType> { columnDataTypeInt, columnDataTypeText, columnDataTypeInt, columnDataTypeInt, columnDataTypeInt, columnDataTypePrice };
            dataGrid.DataTable = new System.Data.DataTable();
            dataGrid.DataTable.Columns.Add();
            dataGrid.DataTable.Columns.Add();
            dataGrid.DataTable.Columns.Add();
            dataGrid.DataTable.Columns.Add();
            dataGrid.DataTable.Columns.Add();
            dataGrid.DataTable.Columns.Add();
            DataRow row = dataGrid.DataTable.NewRow();
            row[0] = "#";
            row[1] = "1";
            row[2] = "Toad.png;cute.png;Toad.png";
            row[3] = "57";
            row[4] = "Status;Teste";
            row[5] = "Teste;Delete;#";
            dataGrid.DataTable.Rows.Add(row);
            DataRow row2 = dataGrid.DataTable.NewRow();
            row2[0] = "#";
            row2[1] = "qwewqewqe";
            row2[2] = "Toad.png;Toad.png";
            row2[3] = "10";
            row2[4] = "Status;Completed;Teste";
            row2[5] = "Teste;#";
            dataGrid.DataTable.Rows.Add(row2);
            DataRow row3 = dataGrid.DataTable.NewRow();
            row3[0] = "#";
            row3[1] = "ffff";
            row3[2] = "Toad.png;Toad.png;cute.png;Toad.png";
            row3[3] = "75";
            row3[4] = "Status";
            row3[5] = "Teste";
            dataGrid.DataTable.Rows.Add(row3);

            List<Bike> bikeList = new List<Bike>();

            DataTable dataTable;
            DataTable dataTableResult = new DataTable();
            dataTableResult.Columns.Add();
            dataTableResult.Columns.Add();
            dataTableResult.Columns.Add();
            dataTableResult.Columns.Add();
            dataTableResult.Columns.Add();
            dataTableResult.Columns.Add();
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://pesti2020-001-site1.itempurl.com/api/values");

                var response = req.GetResponse();
                string webcontent;
                using (var strm = new StreamReader(response.GetResponseStream()))
                {
                    webcontent = strm.ReadToEnd();
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                bikeList = js.Deserialize<List<Bike>>(webcontent);

                if (!string.IsNullOrEmpty(searchTermsReset))
                {
                    HttpContext.Session.SetString(SessionSearch, "");
                    dataGrid.Search = "";
                }

                if (!string.IsNullOrEmpty(searchTerms))
                {
                    bikeList = bikeList.Where(element => element.Product_name.Contains(searchTerms) ||
                                              element.Product_id.ToString().Contains(searchTerms) ||
                                              element.Model_year.ToString().Contains(searchTerms) ||
                                              element.List_price.ToString().Contains(searchTerms) ||
                                              element.Brand_name.Contains(searchTerms) ||
                                              element.Category_name.Contains(searchTerms)).ToList();
                    HttpContext.Session.SetInt32(SessionPageNumber, 0);
                    HttpContext.Session.SetString(SessionSearch, searchTerms);
                    dataGrid.Search = searchTerms;
                }
                else if (!string.IsNullOrEmpty(HttpContext.Session.GetString(SessionSearch)))
                {
                    searchTerms = HttpContext.Session.GetString(SessionSearch);
                    bikeList = bikeList.Where(element => element.Product_name.Contains(searchTerms) ||
                                              element.Product_id.ToString().Contains(searchTerms) ||
                                              element.Model_year.ToString().Contains(searchTerms) ||
                                              element.List_price.ToString().Contains(searchTerms) ||
                                              element.Brand_name.Contains(searchTerms) ||
                                              element.Category_name.Contains(searchTerms)).ToList();
                    dataGrid.Search = searchTerms;
                }


                int? page = 0;
                if (string.IsNullOrEmpty(HttpContext.Session.GetString(SessionName)))
                {
                    HttpContext.Session.SetInt32(SessionPageNumber, 0);
                    HttpContext.Session.SetInt32(SessionPageLength, lastPageLength);
                    HttpContext.Session.SetString(SessionName, "name");
                }
                page = HttpContext.Session.GetInt32(SessionPageNumber);


                if (page == 0)
                {
                    //HtmlDocument doc = new HtmlDocument();
                    //doc.GetElementbyId("previous").InnerHtml = "hidden";
                }

                if (!string.IsNullOrEmpty(pageNumber))
                {
                    if (pageNumber.Equals("next"))
                    {
                        page++;
                    }
                    else if (pageNumber.Equals("previous"))
                    {
                        if (page > 0)
                        {
                            page--;
                        }
                    }
                }
                HttpContext.Session.SetInt32(SessionPageNumber, page.Value);
                dataGrid.PageNumber = page.Value + 1;


                int length;
                int? _length = HttpContext.Session.GetInt32(SessionPageLength);
                if (pageLength == null)
                {
                    //length = lastPageLength;
                    length = _length.Value;
                    HttpContext.Session.SetString(SessionName, "name");
                }
                else
                {
                    HttpContext.Session.SetString(SessionName, "name");
                    length = Convert.ToInt32(pageLength);
                    HttpContext.Session.SetInt32(SessionPageLength, length);
                    lastPageLength = length;
                }
                dataGrid.LastPage = bikeList.Count / length + 1;

                bikeList = bikeList.Skip(page.Value * length).Take(length).ToList();

                dataTable = Utils.ConvertTo<Bike>(bikeList);
                dataGrid.DataTable = dataTable;
            }
            catch (Exception ex)
            {
                return View(ex.ToString());
            }

            return View(dataGrid);
        }

        public IActionResult Details(string id)
        {
            Bike bike = new Bike();

            try
            {
                if (!String.IsNullOrEmpty(id))
                {

                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://joaonice-001-site1.gtempurl.com/api/values");

                    var response = req.GetResponse();
                    string webcontent;
                    using (var strm = new StreamReader(response.GetResponseStream()))
                    {
                        webcontent = strm.ReadToEnd();
                    }

                    DataGrid dataGrid = new DataGrid();
                    List<Bike> bikeList = new List<Bike>();

                    JavaScriptSerializer js = new JavaScriptSerializer();
                    bikeList = js.Deserialize<List<Bike>>(webcontent);

                    if (bikeList!=null && bikeList.Any())
                    {
                        bike = bikeList.FirstOrDefault(k => k.Product_id == Convert.ToInt32(id));
                    }

                }
            }
            catch
            {

            }

            return View(bike);
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
