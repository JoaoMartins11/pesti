﻿namespace WebApplication1.Models
{
    public class ColumnDataType
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int index;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public enum Types
        {
            Image = 1,
            Text = 2,
            TextWithDetails = 3,
            Progress = 4,
            Status = 5,
            Options = 6
        }

        private Types type;

        public Types Type
        {
            get { return type; }
            set { type = value; }
        }

    }
}