﻿using System;
using System.Collections.Generic;
using System.Text;

namespace userControlsModels.Model
{
    public class Bike 
    {
		private int product_id;

		public int Product_id
		{
			get { return product_id; }
			set { product_id = value; }
		}

		private string product_name;

		public string Product_name
		{
			get { return product_name; }
			set { product_name = value; }
		}

		private string brand_name;

		public string Brand_name
		{
			get { return brand_name; }
			set { brand_name = value; }
		}

		private string category_name;

		public string Category_name
		{
			get { return category_name; }
			set { category_name = value; }
		}

		private int model_year;

		public int Model_year
		{
			get { return model_year; }
			set { model_year = value; }
		}

		private float list_price;

		public float List_price
		{
			get { return list_price; }
			set { list_price = value; }
		}



	}
}
