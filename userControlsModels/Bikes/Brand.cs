﻿using System;
using System.Collections.Generic;
using System.Text;

namespace userControlsModels.Bikes
{
    public class Brand
    {
        private int brand_id;

        public int Brand_id
        {
            get { return brand_id; }
            set { brand_id = value; }
        }

        private string brand_name;

        public string Brand_name
        {
            get { return brand_name; }
            set { brand_name = value; }
        }
    }
}
