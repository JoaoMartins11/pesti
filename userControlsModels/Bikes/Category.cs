﻿using System;
using System.Collections.Generic;
using System.Text;

namespace userControlsModels.Bikes
{
    public class Category
    {
        private int category_id;

        public int Category_id
        {
            get { return category_id; }
            set { category_id = value; }
        }

        private string category_name;

        public string Category_name
        {
            get { return category_name; }
            set { category_name = value; }
        }
    }
}
