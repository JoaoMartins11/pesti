﻿using System;
using userControlsModels.Model;

namespace userControlsModels.Bikes
{
    public class Sale
    {

        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public double Price { get; set; }

        public string SaleValue { get { return (Quantity * Price).ToString("#,##.00€"); } }

        public DateTime SaleDate { get; set; }

        public int CustomerID { get; set; }
        public int ProductID { get; set; }

        public string bikeImg
        {
            get
            {
                Random rnd = new Random();
                int img = rnd.Next(1, 4);

                if (img == 1)
                {
                    return "https://cdn.shopify.com/s/files/1/1928/9661/products/Prevelo_Alpha_Two_16_Kids_Bike_Purple_Profile_600x.jpg?v=1552009579";
                }
                else if (img == 2)
                {
                    return "https://www.the-ebikestore.co.uk/images/gx-folding-electric-bike-p20165-13679_image.jpg";
                }
                else
                {
                    return "https://dfp2hfrf3mn0u.cloudfront.net/162/1626307_original_1.jpg";
                }
            }
        }
    }
}
