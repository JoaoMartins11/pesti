﻿namespace userControlsModels.Grids
{
    public class ColumnDataType
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int index;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        public enum Types
        {
            Image = 1,
            Text = 2,
            TextWithDetails = 3,
            Progress = 4,
            Status = 5,
            Options = 6,
            Price = 7,
            Year = 8,
            ID = 9
        }


        public enum Options
        {
            View = 1,
            Edit = 2,
            Delete = 3
        }

        private Types type;

        public Types Type
        {
            get { return type; }
            set { type = value; }
        }

    }
}