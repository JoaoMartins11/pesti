﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace userControlsModels.Grids
{
    public class DataGrid
    {

        private List<ColumnDataType> columnDataTypes;

        public List<ColumnDataType> ColumnDataTypes
        {
            get { return columnDataTypes; }
            set { columnDataTypes = value; }
        }

        private DataTable dataTable;
        public DataTable DataTable
        {
            get { return dataTable; }
            set { dataTable = value; }
        }

        private string search;

        public string Search
        {
            get { return search; }
            set { search = value; }
        }

        private int pageNumber;

        public int PageNumber
        {
            get { return pageNumber; }
            set { pageNumber = value; }
        }

        private int lastPage;

        public int LastPage
        {
            get { return lastPage; }
            set { lastPage = value; }
        }

        private string pname;

        public string PName
        {
            get { return pname; }
            set { pname = value; }
        }

        private string id;

        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        private string category;

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        private string brand;

        public string Brand
        {
            get { return brand; }
            set { brand = value; }
        }

        private string year;

        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        private string price;

        public string Price
        {
            get { return price; }
            set { price = value; }
        }

        private int test;

        public int Test
        {
            get { return test; }
            set { test = value; }
        }
    }

}
