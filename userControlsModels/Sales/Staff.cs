﻿using System;
using userControlsModels.Model;

namespace userControlsModels.Sales
{
    public class Staff
    {

        public int StaffID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ActiveStatus { get; set; }
        public int StoreID { get; set; }
        public int ManagerID { get; set; }

    }
}
