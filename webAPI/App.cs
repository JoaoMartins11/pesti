﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApi
{
    public class App
    {
        private static void OpenSqlConnection()
        {
            string connectionString = GetConnectionString();

            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = connectionString;

                connection.Open();

                Console.WriteLine("State: {0}", connection.State);
                Console.WriteLine("ConnectionString: {0}",
                    connection.ConnectionString);
            }
        }

        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file.
            return "Data Source=SQL5052.site4now.net;Initial Catalog=DB_A55F41_bikeStore;User Id=DB_A55F41_bikeStore_admin;Password=Pesti1920;";
        }

        public List<String> getInfo()
        {
            OpenSqlConnection();
            
            return null;
        }
    }
}