﻿using System;
using System.Data;
using System.Data.SqlClient;
using webAPI.Data;
using userControlsModels.Model;
using System.Web.Http;

namespace webAPI.Controllers
{

    public class BrandController : ApiController
    {
        // GET api/brand
        public DataTable Get()
        {

            DataTable dtBrands = new DataTable("brands");
            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT b.brand_id, b.brand_name
                                     FROM production.brands b";
                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    //execute the SQLCommand
                    SqlDataReader dr = cmd.ExecuteReader();

                    dtBrands.Load(dr);

           

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return dtBrands;
        }

        // GET api/brands/5
        public string Get(int id)
        {
            return "brand";
        }

        // POST api/brands
        public void Post([FromBody]Bike bike)
        {
            //using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.connectionString))
            //{
            //    connection.Open();

            //    SqlCommand command = connection.CreateCommand();
            //    SqlTransaction transaction;

            //    // Start a local transaction.
            //    transaction = connection.BeginTransaction("UpdateBikeTransaction");


            //    command.Connection = connection;
            //    command.Transaction = transaction;

            //    try
            //    {

            //        string price = bike.List_price.ToString("F2");

            //        command.CommandText =
            //            "UPDATE [production].[products] SET[product_name] = '" + bike.Product_name + "',[model_year] =" + bike.Model_year + ",[list_price] = " + price + " WHERE product_id = " + bike.Product_id;
            //        command.ExecuteNonQuery();

            //        //command.CommandText =
            //        //    "UPDATE Table2 Set Field1= '2' Where Field = 'brand'";
            //        //command.ExecuteNonQuery();

            //        //command.CommandText =
            //        //    "UPDATE Table3 Set Field1= '3' Where Field = 'brand'";
            //        //command.ExecuteNonQuery();

            //        transaction.Commit();



            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
            //        Console.WriteLine("  Message: {0}", ex.Message);

            //        // Attempt to roll back the transaction. 
            //        try
            //        {
            //            transaction.Rollback();
            //        }
            //        catch (Exception ex2)
            //        {
            //            // This catch block will handle any errors that may have occurred 
            //            // on the server that would cause the rollback to fail, such as 
            //            // a closed connection.
            //            Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
            //            Console.WriteLine("  Message: {0}", ex2.Message);
            //        }
            //    }
            //}

        }

        // PUT api/brands/5
        public void Put(int id, [FromBody]string brand)
        {
        }

        // DELETE api/brands/5
        public void Delete(int id)
        {
        }
    }
}
