﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using userControlsModels.Bikes;

namespace webAPI.Controllers
{

    public class CategoryController : ApiController
    {
        // GET api/category
        public DataTable Get()
        {

            DataTable dtCategorys = new DataTable("categories");
            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT c.category_id, c.category_name
                                     FROM production.categories c";
                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    //execute the SQLCommand
                    SqlDataReader dr = cmd.ExecuteReader();

                    dtCategorys.Load(dr);

           

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return dtCategorys;
        }

        // GET api/categorys/5
        public string Get(int id)
        {
            return "category";
        }

        // POST api/categorys
        public void Post([FromBody]Category bike)
        {

        }

        // PUT api/categorys/5
        public void Put(int id, [FromBody]string category)
        {
        }

        // DELETE api/categorys/5
        public void Delete(int id)
        {
        }
    }
}
