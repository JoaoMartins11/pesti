﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using userControlsModels.Bikes;

namespace webAPI.Controllers
{

    public class CustomerController : ApiController
    {


        // GET api/categorys/5
        public Customer Get(int id)
        {
            Customer customer = null;

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT [customer_id]
                                  ,[first_name]
                                  ,[last_name]
                                  ,[phone]
                                  ,[email]
                                  ,[street]
                                  ,[city]
                                  ,[state]
                                  ,[zip_code]
                              FROM [DB_A55F41_bikeStore].[sales].[customers]
                              WHERE customer_id=" + id;

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        customer = new Customer
                        {
                            CustomerID = (int)dr[0],
                            FirstName = (string)dr[1],
                            LastName = (string)dr[2],
                            Email = (string)dr[4],
                            Street = (string)dr[5],
                            City = (string)dr[6],
                            State = (string)dr[7],
                            ZipCode = (string)dr[8]
                        };
                        try { customer.Phone = (string)dr[3]; } catch { }
                    }




                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                customer = new Customer { FirstName = ex.Message };
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return customer;
        }

        // POST api/categorys
        public void Post([FromBody]Customer bike)
        {

        }

        // PUT api/categorys/5
        public void Put(int id, [FromBody]string category)
        {
        }

        // DELETE api/categorys/5
        public void Delete(int id)
        {
        }
    }
}
