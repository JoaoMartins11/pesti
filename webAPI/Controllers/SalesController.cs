﻿using System;
using System.Data;
using System.Data.SqlClient;
using webAPI.Data;
using userControlsModels.Model;
using System.Web.Http;
using System.Collections.Generic;
using userControlsModels.Bikes;
using System.Collections.ObjectModel;

namespace webAPI.Controllers
{

    public class SalesController : ApiController
    {
        // GET api/sales
        public string Get(int year)
        {

            string revenue = "0";

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT Sum(r.list_price*r.quantity) Revenue
                    FROM (Select oi.[quantity],oi.[list_price] from [sales].[order_items] oi, [sales].[orders] o 
                    where oi.[order_id] = o.[order_id] AND o.[order_date]>'" + year + "-01-01' and o.[order_date]<'" + (year + 1) + "-01-01') r";


                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    //execute the SQLCommand
                    object result = cmd.ExecuteScalar();


                    revenue = Convert.ToString(result);


                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return revenue;
        }


        public DataTable GetByMonth(int yearByMonth)
        {


            DataTable dtRevenue = new DataTable("revenue");

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT month(r.od) month, Sum(r.lp*r.qt) revenue
   FROM (Select oi.[quantity] qt,oi.[list_price] lp, o.order_date od from [sales].[order_items] oi, [sales].[orders] o 
   where oi.[order_id] = o.[order_id] AND o.[order_date]>'" + yearByMonth + "-01-01' and o.[order_date]<'" + (yearByMonth + 1) + "-01-01') r group by MONTH(r.od) order by Month(r.od)";



                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    //execute the SQLCommand
                    SqlDataReader dr = cmd.ExecuteReader();

                    dtRevenue.Load(dr);



                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return dtRevenue;
        }




        public DataTable GetSalesByYear(int yearSales)
        {



            DataTable dtSales = new DataTable("sales");

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT s.product_name ProductName,s.quantity Quantity, s.list_price Price, s.order_date SaleDate, s.customer_id CustomerID, s.product_id ProductID
                    FROM (Select oi.quantity,oi.list_price, o.order_date, p.product_name, o.customer_id, p.product_id
		            from [sales].[order_items] oi, [sales].[orders] o, [production].[products] p 
		            where oi.[order_id] = o.[order_id] AND o.[order_date]>'" + yearSales + "-01-01' and o.[order_date]<'" + (yearSales + 1) + "-01-01' AND p.product_id= oi.product_id) s";


                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    //execute the SQLCommand
                    SqlDataReader dr = cmd.ExecuteReader();

                    dtSales.Load(dr);

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return dtSales;




            //DataTable dtSales = new DataTable("sales");
            //try
            //{
            //    //sql connection object
            //    using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
            //    {

            //        //retrieve the SQL Server instance version
            //        string query = @"SELECT s.product_id ProductID, s.product_name ProductName, s.quantity Quantity, s.list_price Price, s.customer_id CustomerID, s.order_date SaleDate
            //        FROM (Select oi.quantity,oi.list_price, o.order_date, p.product_id, o.customer_id, p.product_name
            //  from [sales].[order_items] oi, [sales].[orders] o, [production].[products] p 
            //  where oi.[order_id] = o.[order_id] AND o.[order_date]>'" + yearSales + "-01-01' and o.[order_date]<'" +
            //        (yearSales + 1) + "-01-01' AND p.product_id= oi.product_id) s";


            //        //define the SqlCommand object
            //        SqlCommand cmd = new SqlCommand(query, conn);

            //        //open connection
            //        conn.Open();

            //        //execute the SQLCommand
            //        SqlDataReader dr = cmd.ExecuteReader();

            //        dtSales.Load(dr);

            //        //close data reader
            //        dr.Close();

            //        //close connection
            //        conn.Close();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    //display error message
            //   Console.WriteLine("Exception: " + ex.Message);
            //}


            //return dtSales;
        }
    }
}