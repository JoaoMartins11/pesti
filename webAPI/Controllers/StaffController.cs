﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using userControlsModels.Bikes;
using userControlsModels.Sales;

namespace webAPI.Controllers
{

    public class StaffController : ApiController
    {


        // GET api/categorys/5
        public Staff Get(int id)
        {
            Staff Staff = new Staff();

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT * FROM [sales].[staffs] where staff_id =" + id;

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        Staff = new Staff
                        {
                            StaffID = (int)dr[0],
                            FirstName = (string)dr[1],
                            LastName = (string)dr[2],
                            Email = (string)dr[3],
                            ActiveStatus = (string)dr[5].ToString(),
                            StoreID = (int)dr[6]

                        };
                        try
                        {
                            Staff.ManagerID = (int)dr[7];
                        }
                        catch { }
                        try
                        {
                            Staff.Phone = (string)dr[4];
                        }
                        catch { }
                    }

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            { Staff.FirstName = "teste"; }
            return Staff;
        }

        public Staff GetByID(int StaffID)
        {
            Staff Staff = null;

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT * FROM [sales].[staffs] where staff_id =" + StaffID;

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        Staff = new Staff
                        {
                            StaffID = (int)dr[0],
                            FirstName = (string)dr[1],
                            LastName = (string)dr[2],
                            Email = (string)dr[3],
                            ActiveStatus = (string)dr[5],
                            StoreID = (int)dr[6],
                            ManagerID = (int)dr[7]
                        };
                        try { Staff.Phone = (string)dr[4]; } catch { }
                    }

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            { }
            return Staff;
        }

    }

}
