﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using userControlsModels.Bikes;
using userControlsModels.Sales;

namespace webAPI.Controllers
{

    public class StoreController : ApiController
    {


        // GET api/categorys/5
        public Store Get(int id)
        {
            Store Store = new Store();

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT *
                                    FROM [sales].stores
                                    where store_id =" + id;

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        Store = new Store
                        {
                            StoreID = (int)dr[0],
                            StoreName = (string)dr[1],
                            Email = (string)dr[3],
                            Street = (string)dr[4],
                            City = (string)dr[5],
                            State = (string)dr[6],
                            ZipCode = (string)dr[7]
                        };
                        try { Store.Phone = (string)dr[2]; } catch { }
                    }

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                Store.StoreName = ex.Message;
            }
            return Store;
        }
    }
}
