﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;
using webAPI.Data;
using userControlsModels.Model;
using System.Web.Http.Filters;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Threading;
using System.Web.Helpers;
using System.Net;

namespace webAPI.Controllers
{
    public class ValuesController : ApiController
    {

        // GET api/values
        public DataTable Get()
        {
            ApplicationDbContext dbContext = new ApplicationDbContext();


            DataTable dtBrands = new DataTable("bikes");
            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT p.product_id, p.product_name, pb.brand_name, pc.category_name, p.model_year, p.list_price 
                                     FROM production.products p
                                     INNER JOIN production.brands pb ON p.brand_id=pb.brand_id
                                     INNER JOIN production.categories pc ON p.category_id=pc.category_id
                                     ";
                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    //execute the SQLCommand
                    SqlDataReader dr = cmd.ExecuteReader();

                    dtBrands.Load(dr);



                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return dtBrands;
        }

        // GET api/values/5
        public Bike Get(int id)
        {
            Bike bike = null;

            try
            {
                //sql connection object
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.connectionString))
                {

                    //retrieve the SQL Server instance version
                    string query = @"SELECT p.product_id Product_id, p.product_name Product_name, pb.brand_name Brand_name, pc.category_name Category_name, p.model_year Model_year, p.list_price List_price
                                     FROM production.products p
                                     INNER JOIN production.brands pb ON p.brand_id=pb.brand_id
                                     INNER JOIN production.categories pc ON p.category_id=pc.category_id
                                     WHERE p.product_id=" + id;
                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(query, conn);

                    //open connection
                    conn.Open();

                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.Read())
                    {
                        bike = new Bike
                        {
                            Product_id = (int)dr[0],
                            Product_name = (string)dr[1],
                            Brand_name = (string)dr[2],
                            Category_name = (string)dr[3],
                            Model_year = int.Parse(dr[4].ToString()),
                            List_price = (float)(decimal.ToDouble((decimal)dr[5]))
                        };
                    }


                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                bike = new Bike { Product_name = ex.Message, Product_id = 0 };

                //display error message
                Console.WriteLine("Exception: " + ex.Message);
            }
            return bike;
        }

        // POST api/values

        //  [ValidateAntiForgeryTokenAttribute]
        public IHttpActionResult Post([FromBody]Bike bike)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest("Not a valid model");
            }

            using (SqlConnection connection = new SqlConnection(Properties.Settings.Default.connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction;

                // Start a local transaction.
                transaction = connection.BeginTransaction("UpdateBike");


                command.Connection = connection;
                command.Transaction = transaction;

                try
                {
                    string price = bike.List_price.ToString("F2");
                    command.CommandText =
                        "UPDATE [production].[products] SET[product_name] = '" + bike.Product_name + "',[brand_id] = b.brand_id ,[category_id] = c.category_id ,[model_year] = " + bike.Model_year + ",[list_price] = " + price + " FROM production.brands b, production.categories c WHERE[product_id]= 1 AND b.brand_name = '" + bike.Brand_name + "' AND c.category_name = '" + bike.Category_name + "'";
                    command.ExecuteNonQuery();

                    transaction.Commit();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Commit Exception Type: {0}", ex.GetType());
                    Console.WriteLine("  Message: {0}", ex.Message);

                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Console.WriteLine("Rollback Exception Type: {0}", ex2.GetType());
                        Console.WriteLine("  Message: {0}", ex2.Message);
                    }

                }
            }
            return Ok(bike);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }



        [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
        public sealed class ValidateAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
        {

            public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
            {
                try
                {
                    AntiForgery.Validate();
                }
                catch
                {
                    actionContext.Response = new HttpResponseMessage
                    {
                        StatusCode = HttpStatusCode.Forbidden,
                        RequestMessage = actionContext.ControllerContext.Request
                    };
                    return FromResult(actionContext.Response);
                }
                return continuation();
            }

            private Task<HttpResponseMessage> FromResult(HttpResponseMessage result)
            {
                var source = new TaskCompletionSource<HttpResponseMessage>();
                source.SetResult(result);
                return source.Task;
            }

        }
    }
}
