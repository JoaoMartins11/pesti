﻿using System.Data.Entity;

namespace webAPI.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() :
          base("myConnectionString")
        {

        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

   //     public DbSet<Brand> Brands { get; set; }
    }
}