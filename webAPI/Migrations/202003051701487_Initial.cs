﻿namespace webAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.brands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);   
        }
        
        public override void Down()
        {
            DropTable("dbo.brands");
        }
    }
}
