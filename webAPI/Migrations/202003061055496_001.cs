﻿namespace webAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _001 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Brands", "Status", c => c.String(nullable: false));
            AddColumn("dbo.Brands", "TextWithDetail", c => c.String(nullable: false));
            AddColumn("dbo.Brands", "Progress", c => c.String(nullable: false));
            AddColumn("dbo.Brands", "Options", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Brands", "Options");
            DropColumn("dbo.Brands", "Progress");
            DropColumn("dbo.Brands", "TextWithDetail");
            DropColumn("dbo.Brands", "Status");
        }
    }
}
